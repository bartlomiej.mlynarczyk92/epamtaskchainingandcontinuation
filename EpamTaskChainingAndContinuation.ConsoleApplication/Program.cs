﻿using EpamTaskChainingAndContinuation.Library;


TaskChainingAndContinuation tasks = new TaskChainingAndContinuation();

Random r = new Random();

var average = await Task.Run(() => tasks.CreateArray(10))
               .ContinueWith(array =>
               {
                   Console.WriteLine($"Result of creating array:");
                   foreach (var item in array.Result.AsEnumerable())
                   {
                       Console.WriteLine($"{item.ToString()}");
                   }
                   Console.WriteLine();

                   return tasks.InitializeWithRandom(array.Result);
               })
               .ContinueWith(initializatedArray =>
               {
                   Console.WriteLine($"Result of initializing array:");
                   foreach (var item in initializatedArray.Result.AsEnumerable())
                   {
                       Console.WriteLine($"{item.ToString()}");
                   }
                   Console.WriteLine();
                   var multiplier = r.Next();
                   Console.WriteLine($"Multiplier: {multiplier}");
                   return tasks.multiplyArray(initializatedArray.Result, multiplier);
               })
               .ContinueWith(multipliedArray =>
               {
                   Console.WriteLine($"Result of multiplying array:");
                   foreach (var item in multipliedArray.Result.AsEnumerable())
                   {
                       Console.WriteLine($"{item.ToString()}");
                   }
                   Console.WriteLine();
                   return tasks.sortArrayAscending(multipliedArray.Result);
               })
               .ContinueWith(result =>
               {
                   Console.WriteLine($"Result of sorting array:");
                   foreach (var item in result.Result.AsEnumerable())
                   {
                       Console.WriteLine($"{item.ToString()}");
                   }
                   Console.WriteLine();
                   return tasks.calculateAverage(result.Result);
               });

Console.WriteLine($"average : {average}");
Console.ReadLine();