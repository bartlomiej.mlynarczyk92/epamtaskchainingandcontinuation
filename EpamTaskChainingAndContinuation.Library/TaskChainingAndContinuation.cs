﻿namespace EpamTaskChainingAndContinuation.Library
{
    public class TaskChainingAndContinuation
    {

        //Task 1: creates an array of 10 random integers.
        //Task 2: multiplies the array by a randomly generated number.
        //Task 3: sorts the array by ascending.
        //Task 4: calculates the average value
        

        public async Task<decimal> calculateAverageWithTaskChaining(int arraySize)
        {
            var r = new Random();
            var average = await Task.Run(() => this.CreateArray(arraySize))
                 .ContinueWith(array => this.InitializeWithRandom(array.Result))
                 .ContinueWith(initializatedArray => this.multiplyArray(initializatedArray.Result, r.Next()))
                 .ContinueWith(multipliedArray => this.sortArrayAscending(multipliedArray.Result))
                 .ContinueWith(result => this.calculateAverage(result.Result));

            return average;
        }
        public int[] CreateArray(int size = 10)
        {
            if (size < 1)
            {
                throw new ArgumentException($"Array cannot be smaller then 1!", nameof(size));
            }

            return new int[size];
        }

        public int[] InitializeWithRandom(int[] array)
        {
            validateArray<int[]>(array);
            var r = new Random();
            for (var i = 0; i < array.Length; i++)
            {
                array[i] = r.Next();
            }
            return array;
        }

        public long[] multiplyArray(int[] array, int multiplier)
        {

            validateArray<int[]>(array);
            var resultArray = new long[array.Length];
            for (var i = 0; i < array.Length; i++)
            {
                checked
                {
                    resultArray[i] = array[i] * (long)multiplier;
                }
            }
            return resultArray;
        }

        public long[] sortArrayAscending(long[] array)
        {
            validateArray<long[]>(array);
            var sortedArray = new long[array.Length];
            Array.Copy(array, 0, sortedArray, 0, array.Length);
            Array.Sort(sortedArray, 0, sortedArray.Length);
            return sortedArray;
        }

        public decimal calculateAverage(long[] array)
        {
            validateArray<long[]>(array);
            var average = Convert.ToDecimal(array[0]);
            for (var i = 1; i < array.Length; i++)
            {
                checked
                {
                    average = (average + array[i]) / 2;
                }
            }
            return average;
        }

        private void validateArray<T>(T array)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }
        }

    }
}